const MessageModel = require('../models/Message');

module.exports = {
  async create(req, res) {
    const { message, name, userName } = req.body;

    try {
      const newMessage = await MessageModel.create({ message, name, userName })

      return res.status(200).json(newMessage);
    } catch(err) {
      console.log(err);

      return res.status(500).send({ error: err });
    }
  },

  async listByUserName(req, res) {
    const { userName } = req.params;

    try {
      const messages = await MessageModel.findOne({ userName });

      return res.status(200).json(messages);
    } catch(err) {
      return res.status(500).send({ error: err });
    }
  },

  async listAll(_, res) {
    try {
      const messages = await MessageModel.find()

      return res.status(200).json(messages);
    } catch(err) {
      return res.status(500).send({ error: err});
    }
  }
}
