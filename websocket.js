const socketio = require('socket.io');

const connections = [];
const comments = [];
let io;
exports.setupWebsocket = (server) => {
  
io = socketio(server);
io.sockets.on('connection', function(socket) {
  socket.on('create', function(room) {
    socket.join(room);
  });
  socket.on('removeFromRoom', (room) => {
    socket.leave(room);
  });
});


}





exports.sendMessage  =  (message, data, room)=> {
  io.sockets.in(room).emit(message, data);
}
