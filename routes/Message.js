const express = require('express');
const router = express.Router();
const MessageControler = require('../controllers/Message');

router.post('/', MessageControler.create);
router.get('/', MessageControler.listAll);
router.get('/:userName', MessageControler.listByUserName)

module.exports = router;
